vim9script

# Author:      Tim Pope <http://tpo.pe/>
# Version:     2.2
# Repository:  https://github.com/tpope/vim-surround

if exists("g:loaded_vim_surround")
   finish
endif
g:loaded_vim_surround = 1


import '../autoload/repeat.vim' as rpt

var lastdel = ""
var input = ""

# input functions <<<

def GetChar(): string
   var c = getchar()
   c = type(c) == type(0) ? nr2char(c) : c
   if c =~ '^\d\+$'
      c = nr2char(c)
   endif
   return c
enddef

def InputTarget(): string
   var c = GetChar()
   while c =~ '^\d\+$'
      c = c .. GetChar()
   endwhile
   if c == " "
      c = c .. GetChar()
   endif
   if c =~ "\<Esc>\|\<C-C>\|\0"
      return ""
   else
      return c
   endif
enddef

def InputReplacement(): string
   var c = GetChar()
   if c == " "
      c = c .. GetChar()
   endif
   if c =~ "\<Esc>" || c =~ "\<C-C>"
      return ""
   else
      return c
   endif
enddef

def Beep(): string
   exe "norm! \<Esc>"
   return ""
enddef

def Redraw(): string
   redraw
   return ""
enddef

# >>>

# Wrapping functions <<<

def ExtractBefore(str: string): string
   if str =~ '\r'
      return matchstr(str, '.*\ze\r')
   else
      return matchstr(str, '.*\ze\n')
   endif
enddef

def ExtractAfter(str: string): string
   if str =~ '\r'
      return matchstr(str, '\r\zs.*')
   else
      return matchstr(str, '\n\zs.*')
   endif
enddef

def FixIndent(lstr: string, lspc: string): string
   var str = substitute(lstr, '\t', repeat(' ', &sw), 'g')
   var spc = substitute(lspc, '\t', repeat(' ', &sw), 'g')
   str = substitute(str, '\(\n\|\%^\).\@=', '\1' .. spc, 'g')
   if ! &et
      str = substitute(str, '\s\{' .. &ts .. '\}', "\t", 'g')
   endif
   return str
enddef

def Process(string: string): string
   var repl = []
   for i in range(7)
      repl[i] = ''
      var m = matchstr(string, nr2char(i) .. '.\{-\}\ze' .. nr2char(i))
      if m != ''
         m = substitute(strpart(m, 1), '\r.*', '', '')
         repl[i] = input(match(m, '\w\+$') >= 0 ? m .. ': ' : m)
      endif
   endfor
   var s = ""
   var i = 0
   while i < strlen(string)
      var char = strpart(string, i, 1)
      if char2nr(char) < 8
         var next = stridx(string, char, i + 1)
         if next == -1
            s = s .. char
         else
            var insertion = repl[char2nr(char)]
            var subs = strpart(string, i + 1, next - i - 1)
            subs = matchstr(subs, '\r.*')
            while subs =~ '^\r.*\r'
               var sub = matchstr(subs, "^\r\\zs[^\r]*\r[^\r]*")
               subs = strpart(subs, strlen(sub) + 1)
               var r = stridx(sub, "\r")
               insertion = substitute(insertion, strpart(sub, 0, r), strpart(sub, r + 1), '')
            endwhile
            s = s .. insertion
            i = next
         endif
      else
         s = s .. char
      endif
      i = i + 1
   endwhile
   return s
enddef

def Wrap(string: string, char: string, stype: string, removed: string, special: bool): string
   var keeper = string
   var newchar = char
   var type = stype
   var linemode = type ==# 'V' ? 1 : 0
   var before = ""
   var after  = ""
   var initspaces = ''
   if type ==# "V"
      initspaces = matchstr(keeper, '\%^\s*')
   else
      initspaces = matchstr(getline('.'), '\%^\s*')
   endif
   var pairs = "b()B{}r[]a<>"
   var extraspace = ""
   if newchar =~ '^ '
      newchar = strpart(newchar, 1)
      extraspace = ' '
   endif
   var idx = stridx(pairs, newchar)
   var all = ''
   if newchar == ' '
      before = ''
      after  = ''
   elseif newchar ==# "p"
      before = "\n"
      after  = "\n\n"
   elseif newchar ==# 's'
      before = ' '
      after  = ''
   elseif newchar ==# ':'
      before = ':'
      after = ''
   elseif newchar =~# "[tT\<C-T><]"
      var dounmapp = 0
      var dounmapb = 0
      if !maparg(">", "c")
         dounmapb = 1
         # Hide from AsNeeded
         exe "cn" .. "oremap > ><CR>"
      endif
      var default = ""
      if newchar ==# "T"
         default = matchstr(lastdel, '<\zs.\{-\}\ze>')
      endif
      var tag = input("<", default)
      if dounmapb
         silent! cunmap >
      endif
      input = tag
      if tag != ""
         var keepAttributes = ( match(tag, ">$") == -1 )
         tag = substitute(tag, '>*$', '', '')
         var attributes = ""
         if keepAttributes
            attributes = matchstr(removed, '<[^ \t\n]\+\zs\_.\{-\}\ze>')
         endif
         input = tag .. '>'
         if tag =~ '/$'
            tag = substitute(tag, '/$', '', '')
            before = '<' .. tag .. attributes .. ' />'
            after = ''
         else
            before = '<' .. tag .. attributes .. '>'
            after  = '</' .. substitute(tag, ' .*', '', '') .. '>'
         endif
         if newchar == "\<C-T>"
            if type ==# "v" || type ==# "V"
               before = before .. "\n\t"
            endif
            if type ==# "v"
               after  = "\n" .. after
            endif
         endif
      endif
   elseif newchar ==# 'l' || newchar == '\'
      # LaTeX
      var env = input('\begin{')
      if env != ""
         input = env .. "\<CR>"
         env = '{' .. env
         env = env .. CloseMatch(env)
         echo '\begin' .. env
         before = '\begin' .. env
         after  = '\end' .. matchstr(env, '[^}]*') .. '}'
      endif
   elseif newchar ==# 'f' || newchar ==# 'F'
      var fnc = input('function: ')
      if fnc != ""
         input = fnc .. "\<CR>"
         before = substitute(fnc, '($', '', '') .. '('
         after  = ')'
         if newchar ==# 'F'
            before = before .. ' '
            after = ' ' .. after
         endif
      endif
   elseif newchar ==# "\<C-F>"
      var fnc = input('function: ')
      input = fnc .. "\<CR>"
      before = '(' .. fnc .. ' '
      after = ')'
   elseif idx >= 0
      var spc = (idx % 3) == 1 ? " " : ""
      idx = idx / 3 * 3
      before = strpart(pairs, idx + 1, 1) .. spc
      after  = spc .. strpart(pairs, idx + 2, 1)
   elseif newchar == "\<C-[>" || newchar == "\<C-]>"
      before = "{\n\t"
      after  = "\n}"
   elseif newchar !~ '\a'
      before = newchar
      after  = newchar
   else
      before = ''
      after  = ''
   endif
   after = substitute(after, '\n', '\n' .. initspaces, 'g')
   if type ==# 'V' || (special && type ==# "v")
      before = substitute(before, ' \+$', '', '')
      after  = substitute(after, '^ \+', '', '')
      if after !~ '^\n'
         after  = initspaces .. after
      endif
      if keeper !~ '\n$' && after !~ '^\n'
         keeper = keeper .. "\n"
      elseif keeper =~ '\n$' && after =~ '^\n'
         after = strpart(after, 1)
      endif
      if keeper !~ '^\n' && before !~ '\n\s*$'
         before = before .. "\n"
         if special
            before = before .. "\t"
         endif
      elseif keeper =~ '^\n' && before =~ '\n\s*$'
         keeper = strcharpart(keeper, 1)
      endif
      if type ==# 'V' && keeper =~ '\n\s*\n$'
         keeper = strcharpart(keeper, 0, strchars(keeper) - 1)
      endif
   endif
   if type ==# 'V'
      before = initspaces .. before
   endif
   if before =~ '\n\s*\%$'
      if type ==# 'v'
         keeper = initspaces .. keeper
      endif
      var padding = matchstr(before, '\n\zs\s\+\%$')
      before  = substitute(before, '\n\s\+\%$', '\n', '')
      keeper = FixIndent(keeper, padding)
   endif
   if type ==# 'V'
      keeper = before .. keeper .. after
   elseif type =~ "^\<C-V>"
      # Really we should be iterating over the buffer
      var repl = substitute(before, '[\\~]', '\\&', 'g') .. '\1' .. substitute(after, '[\\~]', '\\&', 'g')
      repl = substitute(repl, '\n', ' ', 'g')
      keeper = substitute(keeper .. "\n", '\(.\{-\}\)\(\n\)', repl .. '\n', 'g')
      keeper = substitute(keeper, '\n\%$', '', '')
   else
      keeper = before .. extraspace .. keeper .. extraspace .. after
   endif
   return keeper
enddef

def WrapReg(reg: string, char: string, removed: string, special: bool)
   var orig = getreg(reg)
   var type = substitute(getregtype(reg), '\d\+$', '', '')
   var new = Wrap(orig, char, type, removed, special)
   setreg(reg, new, type)
enddef

# >>>

def Insert(...args: list<any>): string # <<<
   # Optional argument causes the result to appear on 3 lines, not 1
   var linemode = len(args) > 0 ? args[0] : 0
   var char = InputReplacement()
   while char == "\<CR>" || char == "\<C-S>"
      # TODO: use total count for additional blank lines
      linemode = linemode + 1
      char = InputReplacement()
   endwhile
   if char == ""
      return ""
   endif
   var cb_save = &clipboard
   set clipboard-=unnamed clipboard-=unnamedplus
   var reg_save = getreg()
   setreg('"', "\032", 'v')
   WrapReg('"', char, "", linemode)
   # If line mode is used and the surrounding consists solely of a suffix,
   # remove the initial newline.  This fits a use case of mine but is a
   # little inconsistent.  Is there anyone that would prefer the simpler
   # behavior of just inserting the newline?
   if linemode && match(getreg('"'), '^\n\s*\zs.*') == 0
      setreg('"', matchstr(getreg('"'), '^\n\s*\zs.*'), getregtype('"'))
   endif
   # This can be used to append a placeholder to the end
   if exists("g:surround_insert_tail")
      setreg('"', g:surround_insert_tail, "a" .. getregtype('"'))
   endif
   if &ve != 'all' && col('.') >= col('$')
      if &ve == 'insert'
         var extra_cols = virtcol('.') - virtcol('$')
         if extra_cols > 0
            var [regval, regtype] = [getreg('"', 1, 1), getregtype('"')]
            setreg('"', join(map(range(extra_cols), '" "'), ''), 'v')
            norm! ""p
            setreg('"', regval, regtype)
         endif
      endif
      norm! ""p
   else
      norm! ""P
   endif
   if linemode
      ReIndent()
   endif
   norm! `]
   search("\032", 'bW')
   @@ = reg_save
   &clipboard = cb_save
   return "\<Del>"
enddef # >>>

def ReIndent() # <<<
   if get(b:, 'surround_indent', get(g:, 'surround_indent', 1)) && (!empty(&equalprg) || !empty(&indentexpr) || &cindent || &smartindent || &lisp)
      silent norm! '[=']
   endif
enddef # >>>

def DoSurround(...args: list<any>): string # <<<
   var sol_save = &startofline
   set startofline
   var scount = v:count1
   var char = (len(args) > 0 ? args[0] : InputTarget())
   var spc = 0
   if char =~ '^\d\+'
      scount = scount * char2nr(matchstr(char, '^\d\+'))
      char = substitute(char, '^\d\+', '', '')
   endif
   if char =~ '^ '
      char = strpart(char, 1)
      spc = 1
   endif
   if char == 'a'
      char = '>'
   endif
   if char == 'r'
      char = ']'
   endif
   var newchar = ""
   if len(args) > 1
      newchar = args[1]
      if newchar == "\<Esc>" || newchar == "\<C-C>" || newchar == ""
         if !sol_save
            set nostartofline
         endif
         return Beep()
      endif
   endif
   var cb_save = &clipboard
   set clipboard-=unnamed clipboard-=unnamedplus
   var append = ""
   var original = getreg('"')
   var otype = getregtype('"')
   setreg('"', "")
   var strcount = (scount == 1 ? "" : scount)
   if char == '/'
      exe 'norm! ' .. strcount .. '[/d' .. strcount .. ']/'
   elseif char =~# '[[:punct:][:space:]]' && char !~# '[][(){}<>"''`]'
      exe 'norm! T' .. char
      if getline('.')[col('.') - 1] == char
         exe 'norm! l'
      endif
      exe 'norm! dt' .. char
   else
      exe 'norm! d' .. strcount .. 'i' .. char
   endif
   var keeper = getreg('"')
   var okeeper = keeper # for reindent below
   if keeper == ""
      setreg('"', original, otype)
      &clipboard = cb_save
      if !sol_save
         set nostartofline
      endif
      return ""
   endif
   var oldline = getline('.')
   var oldlnum = line('.')
   if char ==# "p"
      setreg('"', '', 'V')
   elseif char ==# "s" || char ==# "w" || char ==# "W"
      # Do nothing
      setreg('"', '')
   elseif char =~ "[\"'`]"
      exe "norm! i \<Esc>d2i" .. char
      setreg('"', substitute(getreg('"'), ' ', '', ''))
   elseif char == '/'
      norm! "_x
      setreg('"', '/**/', "c")
      keeper = substitute(substitute(keeper, '^/\*\s\=', '', ''), '\s\=\*$', '', '')
   elseif char =~# '[[:punct:][:space:]]' && char !~# '[][(){}<>]'
      exe 'norm! F' .. char
      exe 'norm! df' .. char
   else
      # One character backwards
      call search('\m.', 'bW')
      exe "norm! da" .. char
   endif
   var removed = getreg('"')
   var rem2 = substitute(removed, '\n.*', '', '')
   var oldhead = strpart(oldline, 0, strlen(oldline) - strlen(rem2))
   var oldtail = strpart(oldline, strlen(oldline) - strlen(rem2))
   var regtype = getregtype('"')
   if char =~# '[\[({<T]' || spc
      keeper = substitute(keeper, '^\s\+', '', '')
      keeper = substitute(keeper, '\s\+$', '', '')
   endif
   var pcmd = ''
   if col("']") == col("$") && virtcol('.') + 1 == virtcol('$')
      if oldhead =~# '^\s*$' && len(args) < 2
         keeper = substitute(keeper, '\%^\n' .. oldhead .. '\(\s*.\{-\}\)\n\s*\%$', '\1', '')
      endif
      pcmd = "p"
   else
      pcmd = "P"
   endif
   if line('.') + 1 < oldlnum && regtype ==# "V"
      pcmd = "p"
   endif
   setreg('"', keeper, regtype)
   if newchar != ""
      var special = len(args) > 2 ? args[2] : 0
      WrapReg('"', newchar, removed, special)
   endif
   silent exe 'norm! ""' .. pcmd .. '`['
   if removed =~ '\n' || okeeper =~ '\n' || getreg('"') =~ '\n'
      ReIndent()
   endif
   if getline('.') =~ '^\s\+$' && keeper =~ '^\s*\n'
      silent norm! cc
   endif
   setreg('"', original, otype)
   lastdel = removed
   &clipboard = cb_save
   if newchar == ""
      silent! call rpt.Set("\<Plug>Dsurround" .. char, scount)
   else
      silent! call rpt.Set("\<Plug>C" .. (len(args) > 2 && args[2] ? "S" : "s") .. "urround" .. char .. newchar .. input, scount)
   endif
   if !sol_save
      set nostartofline
   endif
   return ""
enddef # >>>

def ChangeSurround(...args: list<any>): string # <<<
   var a = InputTarget()
   if a == ""
      return Beep()
   endif
   var b = InputReplacement()
   if b == ""
      return Beep()
   endif
   DoSurround(a, b, len(args) > 0 && args[0])
   return ""
enddef # >>>

def Opfunc(type: string, ...args: list<any>): string # <<<
   if type ==# 'setup'
      &opfunc = Opfunc
      return 'g@'
   endif
   var char = InputReplacement()
   if char == ""
      return Beep()
   endif
   var reg = '"'
   var sel_save = &selection
   &selection = "inclusive"
   var cb_save  = &clipboard
   set clipboard-=unnamed clipboard-=unnamedplus
   var reg_save = getreg(reg)
   var reg_type = getregtype(reg)
   var stype = type
   if type == "char"
      silent exe 'norm! v`[o`]"' .. reg .. 'y'
      stype = 'v'
   elseif type == "line"
      silent exe 'norm! `[V`]"' .. reg .. 'y'
      stype = 'V'
   elseif type ==# "v" || type ==# "V" || type ==# "\<C-V>"
      &selection = sel_save
      var ve = &virtualedit
      if !(len(args) > 0 && args[0])
         set virtualedit=
      endif
      silent exe 'norm! gv"' .. reg .. 'y'
      &virtualedit = ve
   elseif type =~ '^\d\+$'
      stype = 'v'
      silent exe 'norm! ^v' .. type .. '$h"' .. reg .. 'y'
      if mode() ==# 'v'
         norm! v
         return Beep()
      endif
   else
      &selection = sel_save
      &clipboard = cb_save
      return Beep()
   endif
   var keeper = getreg(reg)
   var append = ''
   if stype ==# "v" && type !=# "v"
      append = matchstr(keeper, '\_s\@<!\s*$')
      keeper = substitute(keeper, '\_s\@<!\s*$', '', '')
   endif
   setreg(reg, keeper, stype)
   WrapReg(reg, char, "", len(args) > 0 && args[0])
   if stype ==# "v" && type !=# "v" && append != ""
      setreg(reg, append, "ac")
   endif
   silent exe 'norm! gv' .. (reg == '"' ? '' : '"' .. reg) .. 'p`['
   if stype ==# 'V' || (getreg(reg) =~ '\n' && stype ==# 'v')
      ReIndent()
   endif
   setreg(reg, reg_save, reg_type)
   &selection = sel_save
   &clipboard = cb_save
   if type =~ '^\d\+$'
      silent! call repeat#set("\<Plug>Y" .. (len(args) > 1 && args[0] ? "S" : "s") .. "surround" .. char .. input, type)
   else
      silent! call repeat#set("\<Plug>SurroundRepeat" .. char .. input)
   endif
   return ''
enddef

def Opfunc2(...args: list<any>): string
   if len(args) == 0 || args[0] ==# 'setup'
      &opfunc = Opfunc2
      return 'g@'
   endif
   Opfunc(args[0], 1)
   return ''
enddef # >>>

def CloseMatch(str: string): string # <<<
   # Close an open (, {, [, or < on the command line.
   var tail = matchstr(str, '.[^\[\](){}<>]*$')
   if tail =~ '^\[.\+'
      return "]"
   elseif tail =~ '^(.\+'
      return ")"
   elseif tail =~ '^{.\+'
      return "}"
   elseif tail =~ '^<.+'
      return ">"
   else
      return ""
   endif
enddef # >>>

nnoremap <silent> <Plug>SurroundRepeat .
nnoremap <silent> <Plug>Dsurround  :<C-U>call <SID>DoSurround(<SID>InputTarget())<CR>
nnoremap <silent> <Plug>Csurround  :<C-U>call <SID>ChangeSurround()<CR>
nnoremap <silent> <Plug>CSurround  :<C-U>call <SID>ChangeSurround(1)<CR>
nnoremap <expr>   <Plug>Yssurround '^' .. v:count1 .. <SID>Opfunc('setup') .. 'g_'
nnoremap <expr>   <Plug>YSsurround <SID>Opfunc2('setup') .. '_'
nnoremap <expr>   <Plug>Ysurround  <SID>Opfunc('setup')
nnoremap <expr>   <Plug>YSurround  <SID>Opfunc2('setup')
vnoremap <silent> <Plug>VSurround  :<C-U>call <SID>Opfunc(visualmode(), visualmode() ==# 'V' ? 1 : 0)<CR>
vnoremap <silent> <Plug>VgSurround :<C-U>call <SID>Opfunc(visualmode(), visualmode() ==# 'V' ? 0 : 1)<CR>
inoremap <silent> <Plug>Isurround  <C-R>=<SID>Insert()<CR>
inoremap <silent> <Plug>ISurround  <C-R>=<SID>Insert(1)<CR>

if !exists("g:surround_no_mappings") || ! g:surround_no_mappings
   nmap ds  <Plug>Dsurround
   nmap cs  <Plug>Csurround
   nmap cS  <Plug>CSurround
   nmap ys  <Plug>Ysurround
   nmap yS  <Plug>YSurround
   nmap yss <Plug>Yssurround
   nmap ySs <Plug>YSsurround
   nmap ySS <Plug>YSsurround
   xmap S   <Plug>VSurround
   xmap gS  <Plug>VgSurround
   if !exists("g:surround_no_insert_mappings") || ! g:surround_no_insert_mappings
      if !hasmapto("<Plug>Isurround", "i") && "" == mapcheck("<C-S>", "i")
         imap    <C-S> <Plug>Isurround
      endif
      imap      <C-G>s <Plug>Isurround
      imap      <C-G>S <Plug>ISurround
   endif
endif

defcompile
# vim: set et sts=3 sw=3 foldmethod=marker foldmarker=<<<,>>>:
