vim9script
# Maps <C-h/j/k/l> to switch vim splits in the given direction. If there are
# no more windows in that direction, forwards the operation to tmux.
# Additionally, <C-\> toggles between last active vim splits/tmux panes.

if exists("g:loaded_tmux_navigator") || &cp || v:version < 700
  finish
endif
g:loaded_tmux_navigator = 1

def VimNavigate(dir: string)
  try
    execute 'wincmd ' .. dir
  catch
    echohl ErrorMsg | echo 'E11: Invalid in command-line window; <CR> executes, CTRL-C quits: wincmd k' | echohl None
  endtry
enddef

if !get(g:, 'tmux_navigator_no_mappings', 0)
  noremap <silent> <c-h> :TmuxNavigateLeft<CR>
  noremap <silent> <c-j> :TmuxNavigateDown<CR>
  noremap <silent> <c-k> :TmuxNavigateUp<CR>
  noremap <silent> <c-l> :TmuxNavigateRight<CR>
  noremap <silent> <c-\> :TmuxNavigatePrevious<CR>
endif

if empty($TMUX)
  command! TmuxNavigateLeft     call <SID>VimNavigate('h')
  command! TmuxNavigateDown     call <SID>VimNavigate('j')
  command! TmuxNavigateUp       call <SID>VimNavigate('k')
  command! TmuxNavigateRight    call <SID>VimNavigate('l')
  command! TmuxNavigatePrevious call <SID>VimNavigate('p')
  finish
endif

command! TmuxNavigateLeft     call <SID>TmuxAwareNavigate('h')
command! TmuxNavigateDown     call <SID>TmuxAwareNavigate('j')
command! TmuxNavigateUp       call <SID>TmuxAwareNavigate('k')
command! TmuxNavigateRight    call <SID>TmuxAwareNavigate('l')
command! TmuxNavigatePrevious call <SID>TmuxAwareNavigate('p')

if !exists("g:tmux_navigator_save_on_switch")
  g:tmux_navigator_save_on_switch = 0
endif

if !exists("g:tmux_navigator_disable_when_zoomed")
  g:tmux_navigator_disable_when_zoomed = 0
endif

if !exists("g:tmux_navigator_preserve_zoom")
  g:tmux_navigator_preserve_zoom = 0
endif

if !exists("g:tmux_navigator_no_wrap")
  g:tmux_navigator_no_wrap = 0
endif

var pane_position_from_direction = {'h': 'left', 'j': 'bottom', 'k': 'top', 'l': 'right'}

def TmuxOrTmateExecutable(): string
  return (match($TMUX, 'tmate') != -1 ? 'tmate' : 'tmux')
enddef

def TmuxVimPaneIsZoomed(): bool
  return TmuxCommand("display-message -p '#{window_zoomed_flag}'") == 1
enddef

def TmuxSocket(): string
  # The socket path is the first value in the comma-separated list of $TMUX.
  return split($TMUX, ',')[0]
enddef

def TmuxCommand(args: string): any
  var cmd = TmuxOrTmateExecutable() .. ' -S ' .. TmuxSocket() .. ' ' .. args
  var x = &shellcmdflag
  &shellcmdflag = '-c'
  var retval = system(cmd)
  &shellcmdflag = x
  return retval
enddef

def TmuxNavigatorProcessList()
  echo TmuxCommand("run-shell 'ps -o state= -o comm= -t ''''#{pane_tty}'''''")
enddef
command! TmuxNavigatorProcessList call <SID>TmuxNavigatorProcessList()

var tmux_is_last_pane = 0
augroup tmux_navigator
  au!
  autocmd WinEnter * tmux_is_last_pane = 0
augroup END

def NeedsVitalityRedraw(): bool
  return exists('g:loaded_vitality') && v:version < 704 && !has("patch481")
enddef

def ShouldForwardNavigationBackToTmux(tmux_last_pane: bool, at_tab_page_edge: bool): bool
  if g:tmux_navigator_disable_when_zoomed && TmuxVimPaneIsZoomed()
    return 0
  endif
  return (tmux_last_pane || at_tab_page_edge)
enddef

def TmuxAwareNavigate(direction: string)
  var nr = winnr()
  var tmux_last_pane = (direction == 'p' && tmux_is_last_pane)
  if !tmux_last_pane
    VimNavigate(direction)
  endif
  var at_tab_page_edge = (nr == winnr())
  # Forward the switch panes command to tmux if:
  # a) we're toggling between the last tmux pane;
  # b) we tried switching windows in vim but it didn't have effect.
  if ShouldForwardNavigationBackToTmux(tmux_last_pane, at_tab_page_edge)
    if g:tmux_navigator_save_on_switch == 1
      try
        update # save the active buffer. See :help update
      catch /^Vim\%((\a\+)\)\=:E32/ # catches the no file name error
      endtry
    elseif g:tmux_navigator_save_on_switch == 2
      try
        wall # save all the buffers. See :help wall
      catch /^Vim\%((\a\+)\)\=:E141/ # catches the no file name error
      endtry
    endif
    var args = 'select-pane -t ' .. shellescape($TMUX_PANE) .. ' -' .. tr(direction, 'phjkl', 'lLDUR')
    if g:tmux_navigator_preserve_zoom == 1
      args = args .. ' -Z'
    endif
    if g:tmux_navigator_no_wrap == 1
      args = 'if -F "#{pane_at_' .. pane_position_from_direction[direction] .. '}" "" "' .. args .. '"'
    endif
    silent call TmuxCommand(args)
    if NeedsVitalityRedraw()
      redraw!
    endif
    tmux_is_last_pane = 1
  else
    tmux_is_last_pane = 0
  endif
enddef

defcompile
# vim: set et sts=2 sw=2:
